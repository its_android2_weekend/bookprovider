package com.example.bookprovider;

import androidx.appcompat.app.AppCompatActivity;
import androidx.loader.content.CursorLoader;

import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private TextView tvBook;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tvBook = findViewById(R.id.tvBook);
        tvBook.setText(ReadBook());
    }
    private String ReadBook() {
        Uri uri = Uri.parse("content://com.example.provider.books/books");
        Cursor cursor;
        CursorLoader loader = new CursorLoader(
                this,
                uri,
                null,
                null,
                null,
                null
        );
        cursor = loader.loadInBackground();
        String str = "";

        while(cursor != null && cursor.moveToNext()){
            String id = cursor.getString(cursor.getColumnIndex("_id"));
            String title = cursor.getString(cursor.getColumnIndex("title"));
            String isbn = cursor.getString(cursor.getColumnIndex("isbn"));

            str = "{ "+id+ ","+title + "," + isbn+"}";
        }
        return str;
    }
}
